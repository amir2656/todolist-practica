using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Collections.Generic;

namespace ToDoList.BLL.Tests
{
    public class ToDoItemManagerTests
    {
        private DbContextOptions<ToDoListDbContext> inMemoryContextOptions =
            new DbContextOptionsBuilder<ToDoListDbContext>().UseInMemoryDatabase("WorkshopToDoList").Options;

        ToDoItemManager managerToTest;

        [SetUp]
        public void Setup()
        {
            var context = new ToDoListDbContext(inMemoryContextOptions);
            managerToTest = new ToDoItemManager(context);
        }

        [Test]
        public void AddItem()
        {
            var addItemDTO = new AddItemDTO
            {
                Description = "Some text here"
            };
            ToDoItem addedItem = managerToTest.AddItem(addItemDTO);

            Assert.AreEqual(addItemDTO.Description, addedItem.Description);
            Assert.AreEqual(ItemStates.New, addedItem.States);
        }

        [Test]
        public void GetAllItems()
        {
            List<ToDoItem> allItems = managerToTest.GetAllItems();
            Assert.IsEmpty(allItems);

            managerToTest.AddItem(new AddItemDTO { Description = "desc1"});
            managerToTest.AddItem(new AddItemDTO { Description = "desc2"});
            managerToTest.AddItem(new AddItemDTO { Description = "desc3"});

            Assert.AreEqual(managerToTest.GetAllItems().Count, 3);
        }

        [Test]
        public void GetItemWithId()
        {
            Assert.IsNull(managerToTest.GetItemWithId(1));

            var item1 = new AddItemDTO { Description = "desc1" };
            var item2 = new AddItemDTO { Description = "desc2" };

            managerToTest.AddItem(item1);
            managerToTest.AddItem(item2);

            Assert.IsNull(managerToTest.GetItemWithId(3));

            Assert.AreEqual(managerToTest.GetItemWithId(1).Description, item1.Description);
            Assert.AreEqual(managerToTest.GetItemWithId(2).Description, item2.Description);
        }

        [Test]
        public void UpdateItem()
        {
            var itemToUpdate = new UpdateItemWithIdDTO { NewDescription = "new Description", NewState = ItemStates.Started };
            Assert.IsNull(managerToTest.UpdateItem(1, itemToUpdate));

            var itemToAdd = new AddItemDTO { Description = "Old description" };
            ToDoItem itemAdded = managerToTest.AddItem(itemToAdd);

            Assert.AreEqual(itemToAdd.Description, itemAdded.Description);

            var itemUpdated = managerToTest.UpdateItem(1, itemToUpdate);
            Assert.AreEqual(itemUpdated.ID, itemAdded.ID);
            Assert.AreEqual(itemUpdated.CreatedAt, itemAdded.CreatedAt);

            Assert.AreEqual(itemUpdated.Description, itemToUpdate.NewDescription);
            Assert.AreEqual(itemUpdated.States, itemToUpdate.NewState);

        }
    }
}