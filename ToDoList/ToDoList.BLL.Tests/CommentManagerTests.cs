﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace ToDoList.BLL.Tests
{
    public class CommentManagerTests
    {
        private DbContextOptions<ToDoListDbContext> inMemoryContextOptions;

        CommentManager commentManagerToTest;
        ToDoItemManager itemManagerToTest;

        [SetUp]
        public void Setup()
        {
            inMemoryContextOptions = new DbContextOptionsBuilder<ToDoListDbContext>().UseInMemoryDatabase("WorkshopToDoList").Options;

            var context = new ToDoListDbContext(inMemoryContextOptions);
            commentManagerToTest = new CommentManager(context);
            itemManagerToTest = new ToDoItemManager(context);

            itemManagerToTest.AddItem(new AddItemDTO { Description = "Description 1"});
            itemManagerToTest.AddItem(new AddItemDTO { Description = "Description 2"});
            itemManagerToTest.AddItem(new AddItemDTO { Description = "Description 3"});

            System.Console.WriteLine("s");
        }

        [Test]
        public void AddComment()
        {
            System.Console.WriteLine("A");

            var commentToAdd = new AddCommentToItemDTO { Body = "Frumos!", ParentPostId = 1 };
            var commentAdded = commentManagerToTest.AddComment(commentToAdd);
            Assert.AreEqual(commentToAdd.Body, commentAdded.Body);
            Assert.AreEqual(commentToAdd.ParentPostId, commentAdded.ParentPostId);


        }

        [Test]
        public void GetAllCommentsOfItem()
        {
            //TODO - repair
            System.Console.WriteLine("B\n");

            var allComments = commentManagerToTest.GetAllCommentsOfItem(2);

            Assert.IsEmpty(allComments);

            commentManagerToTest.AddComment(new AddCommentToItemDTO { Body = "Frumos peisaj!", ParentPostId = 1 });
            commentManagerToTest.AddComment(new AddCommentToItemDTO { Body = "Frumos cantec!", ParentPostId = 1 });
            commentManagerToTest.AddComment(new AddCommentToItemDTO { Body = "Frumos album!", ParentPostId = 1 });
            commentManagerToTest.AddComment(new AddCommentToItemDTO { Body = "Frumoasa haina!", ParentPostId = 3 });

            allComments = commentManagerToTest.GetAllCommentsOfItem(1);

            Assert.AreEqual(3, allComments.Count);

            allComments = commentManagerToTest.GetAllCommentsOfItem(3);

            Assert.AreEqual(1, allComments.Count);

        }

        [Test]
        public void UpdateComment()
        {
            var commentAdded = commentManagerToTest.AddComment(new AddCommentToItemDTO { Body = "Frumos peisaj!", ParentPostId = 1 });

            UpdateCommentWithGuidDTO commentToUpdate = new UpdateCommentWithGuidDTO { NewBody = "Frumos caine!" };
            var updatedComment = commentManagerToTest.UpdateComment(commentAdded.ID, commentToUpdate);

            Assert.AreEqual(commentToUpdate.NewBody, updatedComment.Body);
        }

        [Test]
        public void GetCommentByGuid()
        {
            AddCommentToItemDTO commentToAdd = new AddCommentToItemDTO { Body = "Description", ParentPostId = 1 };
            var commentAdded = commentManagerToTest.AddComment(commentToAdd);

            Comment commentSearched = commentManagerToTest.GetCommentByGuid(commentAdded.ID);
            Assert.AreEqual(commentAdded.Body, commentSearched.Body);
        }
    }
}
