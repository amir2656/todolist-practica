using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ToDoList.BLL;
using System.Collections.Generic;

namespace ToDoList.API
{
    public class ItemFunctions
    {
        private ToDoItemManager itemManager;

        private CommentManager commentManager;

        public ItemFunctions(ToDoListDbContext context)
        {
            itemManager = new ToDoItemManager(context);
            commentManager = new CommentManager(context);
        }

        [FunctionName("AddItem")]
        public ActionResult<ToDoItem> AddItem(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "item")] HttpRequest req,
            ILogger log)
        {
            AddItemDTO addItemData = null;
            try
            {
                string requestBody = new StreamReader(req.Body).ReadToEnd();
                addItemData = JsonConvert.DeserializeObject<AddItemDTO>(requestBody);
                var addedItem = itemManager.AddItem(addItemData);
                return addedItem;

            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        //update
        [FunctionName("UpdateItemWithId")]
        public ActionResult<ToDoItem> UpdateItemWithId(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "item/{id}")] HttpRequest req, int id,
            ILogger log)
        {
            try
            {
                UpdateItemWithIdDTO updateData = null;
                string requestBody = new StreamReader(req.Body).ReadToEnd();
                updateData = JsonConvert.DeserializeObject<UpdateItemWithIdDTO>(requestBody);
                ToDoItem updatedItem = itemManager.UpdateItem(id, updateData);
                return updatedItem;
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        //GetAllItems
        [FunctionName("GetAllItems")]
        public ActionResult<List<ToDoItem>> GetAllItems(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "item")] HttpRequest req,
            ILogger log)
        {
            try
            {
                List<ToDoItem> allItems = itemManager.GetAllItems();
                return allItems;

            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        //getOneItem
        [FunctionName("GetItemWithId")]
        public ActionResult<ToDoItem> GetItemWithId(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "item/{id}")] HttpRequest req, int id,
            ILogger log)
        {
            try
            {
                ToDoItem searchedItem = itemManager.GetItemWithId(id);
                if (searchedItem == null)
                {
                    return new NotFoundResult();
                }
                else
                {
                    return searchedItem;
                }
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        //AddCommentToItem
        [FunctionName("AddCommentToItem")]
        public ActionResult<Comment> AddCommentToItem(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "item/comment")] HttpRequest req,
            ILogger log)
        {
            AddCommentToItemDTO addCommentData = null;
            try
            {
                string requestBody = new StreamReader(req.Body).ReadToEnd();
                addCommentData = JsonConvert.DeserializeObject<AddCommentToItemDTO>(requestBody);
                var addedComment = commentManager.AddComment(addCommentData);
                return addedComment;

            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        //GetAllCommentsOfItem
        [FunctionName("GetAllCommentsOfItem")]
        public ActionResult<List<Comment>> GetAllCommentsOfItem(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "item/{id}/comments")] HttpRequest req, int id,
            ILogger log)
        {
            try
            {
                List<Comment> allCommentsOfItem = commentManager.GetAllCommentsOfItem(id);
                return allCommentsOfItem;

            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        //DeleteCommentByGuid
        //TODO : Delete

        //UpdateCommentByGuid
        [FunctionName("UpdateCommentByGuid")]
        public ActionResult<Comment> UpdateCommentByGuid(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "comment/{guid}")] HttpRequest req, Guid guid,
            ILogger log)
        {
            try
            {
                UpdateCommentWithGuidDTO updateData = null;
                string requestBody = new StreamReader(req.Body).ReadToEnd();
                updateData = JsonConvert.DeserializeObject<UpdateCommentWithGuidDTO>(requestBody);
                Comment updatedComment = commentManager.UpdateComment(guid, updateData);
                return updatedComment;
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        //GetCommentByGuid
        [FunctionName("GetCommentByGuid")]
        public ActionResult<Comment> GetCommentByGuid(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "comment/{guid}")] HttpRequest req, Guid guid,
            ILogger log)
        {
            try
            {
                return commentManager.GetCommentByGuid(guid);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }
    }
}
