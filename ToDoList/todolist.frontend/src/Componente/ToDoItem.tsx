import React from 'react'
import { ToDoItemProps } from '../Types/types'

export function ToDoItem(props:ToDoItemProps){
    return(
        <div>
            <h2>Description: {props.description} </h2>
            <div>Created at: {props.createdAt.toDateString()}</div>
            <div>State: {props.state}</div>
        </div>
    );
}