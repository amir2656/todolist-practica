﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.BLL
{
    public class UpdateCommentWithGuidDTO
    {
        public string NewBody { get; set; }
    }
}
