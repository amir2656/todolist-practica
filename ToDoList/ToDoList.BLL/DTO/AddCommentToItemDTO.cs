﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.BLL
{
    public class AddCommentToItemDTO
    {
        public string Body { get; set; }

        public int ParentPostId { get; set; }
    }
}
