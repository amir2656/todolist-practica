﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.BLL
{
    public class ToDoItemWithComments : ToDoItem
    {
        public List<Comment> comments { get; set; }
    }
}
