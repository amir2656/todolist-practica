﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ToDoList.BLL
{
    public class Comment
    {
        [Key]
        public Guid ID { get; set; }

        public string Body { get; set; }

        public int ParentPostId { get; set; }
    }
}
