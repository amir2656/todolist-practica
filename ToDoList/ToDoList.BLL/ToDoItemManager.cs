﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToDoList.BLL
{
    public class ToDoItemManager
    {
        private readonly ToDoListDbContext _context;

        public ToDoItemManager(ToDoListDbContext context)
        {
            _context = context;
        }

        public ToDoItem AddItem(AddItemDTO addItemData)
        {
            ToDoItem newItemToAdd = new ToDoItem
            {
                CreatedAt = DateTime.Now,
                Description = addItemData.Description,
                States = ItemStates.New
            };

            _context.Add(newItemToAdd);
            _context.SaveChanges();
            return newItemToAdd;
        }

        public List<ToDoItem> GetAllItems()
        {
            List<ToDoItem> toDoItems = new List<ToDoItem>();
            foreach (ToDoItem item in _context.ToDoItemsData)
            {
                List<Comment> commentsListOfItem = _context.CommentsData.Where((e) => e.ParentPostId.Equals(item.ID)).ToList();
                if (commentsListOfItem.Count != 0)
                {
                    ToDoItemWithComments itemWithComments = new ToDoItemWithComments
                    {
                        ID = item.ID,
                        CreatedAt = item.CreatedAt,
                        Description = item.Description,
                        States = item.States,
                        UserName = item.UserName,
                        comments = commentsListOfItem
                    };
                    toDoItems.Add(itemWithComments);
                }
                else
                {
                    toDoItems.Add(item);
                }
            }
            return toDoItems;
        }

        public ToDoItem GetItemWithId(int id)
        {
            ToDoItem itemSearched = _context.ToDoItemsData.Find(id);
            if (itemSearched != null)
            {
                List<Comment> commentsListOfItem = _context.CommentsData.Where((e) => e.ParentPostId.Equals(itemSearched.ID)).ToList();
                if (commentsListOfItem.Count != 0)
                {
                    itemSearched = new ToDoItemWithComments
                    {
                        ID = itemSearched.ID,
                        CreatedAt = itemSearched.CreatedAt,
                        Description = itemSearched.Description,
                        States = itemSearched.States,
                        UserName = itemSearched.UserName,
                        comments = commentsListOfItem
                    };
                }

            }
            return itemSearched;
        }

        public ToDoItem UpdateItem(int id, UpdateItemWithIdDTO updateData)
        {
            ToDoItem itemToUpdate = _context.ToDoItemsData.Find(id);
            if (itemToUpdate != null)
            {
                List<Comment> commentsListOfItem = _context.CommentsData.Where((e) => e.ParentPostId.Equals(itemToUpdate.ID)).ToList();
                if (commentsListOfItem.Count != 0)
                {
                    itemToUpdate = new ToDoItemWithComments
                    {
                        ID = itemToUpdate.ID,
                        CreatedAt = itemToUpdate.CreatedAt,
                        Description = itemToUpdate.Description,
                        States = itemToUpdate.States,
                        UserName = itemToUpdate.UserName,
                        comments = commentsListOfItem
                    };
                }
                itemToUpdate.Description = updateData.NewDescription;
                itemToUpdate.States = updateData.NewState;
                _context.SaveChanges();
            }
            return itemToUpdate;
        }
    }
}
