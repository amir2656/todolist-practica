﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoList.BLL.Migrations
{
    public partial class CommentsAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CommentData",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Body = table.Column<string>(nullable: true),
                    ToDoItemID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentData", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CommentData_ToDoItemsData_ToDoItemID",
                        column: x => x.ToDoItemID,
                        principalTable: "ToDoItemsData",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommentData_ToDoItemID",
                table: "CommentData",
                column: "ToDoItemID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentData");
        }
    }
}
