﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoList.BLL.Migrations
{
    public partial class BiDrcItemComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentData_ToDoItemsData_ToDoItemID",
                table: "CommentData");

            migrationBuilder.DropIndex(
                name: "IX_CommentData_ToDoItemID",
                table: "CommentData");

            migrationBuilder.DropColumn(
                name: "ToDoItemID",
                table: "CommentData");

            migrationBuilder.AddColumn<int>(
                name: "ParentPostID",
                table: "CommentData",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CommentData_ParentPostID",
                table: "CommentData",
                column: "ParentPostID");

            migrationBuilder.AddForeignKey(
                name: "FK_CommentData_ToDoItemsData_ParentPostID",
                table: "CommentData",
                column: "ParentPostID",
                principalTable: "ToDoItemsData",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentData_ToDoItemsData_ParentPostID",
                table: "CommentData");

            migrationBuilder.DropIndex(
                name: "IX_CommentData_ParentPostID",
                table: "CommentData");

            migrationBuilder.DropColumn(
                name: "ParentPostID",
                table: "CommentData");

            migrationBuilder.AddColumn<int>(
                name: "ToDoItemID",
                table: "CommentData",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CommentData_ToDoItemID",
                table: "CommentData",
                column: "ToDoItemID");

            migrationBuilder.AddForeignKey(
                name: "FK_CommentData_ToDoItemsData_ToDoItemID",
                table: "CommentData",
                column: "ToDoItemID",
                principalTable: "ToDoItemsData",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
