﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoList.BLL.Migrations
{
    public partial class FixingBugParentItemComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentData_ToDoItemsData_ParentPostID",
                table: "CommentData");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CommentData",
                table: "CommentData");

            migrationBuilder.DropIndex(
                name: "IX_CommentData_ParentPostID",
                table: "CommentData");

            migrationBuilder.RenameTable(
                name: "CommentData",
                newName: "CommentsData");

            migrationBuilder.RenameColumn(
                name: "ParentPostID",
                table: "CommentsData",
                newName: "ParentPostId");

            migrationBuilder.AlterColumn<int>(
                name: "ParentPostId",
                table: "CommentsData",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CommentsData",
                table: "CommentsData",
                column: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CommentsData",
                table: "CommentsData");

            migrationBuilder.RenameTable(
                name: "CommentsData",
                newName: "CommentData");

            migrationBuilder.RenameColumn(
                name: "ParentPostId",
                table: "CommentData",
                newName: "ParentPostID");

            migrationBuilder.AlterColumn<int>(
                name: "ParentPostID",
                table: "CommentData",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_CommentData",
                table: "CommentData",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_CommentData_ParentPostID",
                table: "CommentData",
                column: "ParentPostID");

            migrationBuilder.AddForeignKey(
                name: "FK_CommentData_ToDoItemsData_ParentPostID",
                table: "CommentData",
                column: "ParentPostID",
                principalTable: "ToDoItemsData",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
