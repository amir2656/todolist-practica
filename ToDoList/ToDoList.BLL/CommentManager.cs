﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToDoList.BLL
{
    public class CommentManager
    {
        public ToDoListDbContext _context;

        public CommentManager(ToDoListDbContext context)
        {
            _context = context;
        }

        public Comment AddComment(AddCommentToItemDTO addCommentData)
        {
            //TODO : Verify if ParentPostId is valid
            Comment newComment = new Comment
            {
                Body = addCommentData.Body,
                ParentPostId = addCommentData.ParentPostId
            };
            _context.Add(newComment);
            _context.SaveChanges();

            return newComment;
        }

        public List<Comment> GetAllCommentsOfItem(int id)
        {
            List<Comment> allComments = _context.CommentsData.Where((comment)=>comment.ParentPostId.Equals(id)).ToList();
            return allComments;
        }

        public Comment UpdateComment(Guid guid, UpdateCommentWithGuidDTO updateData)
        {
            Comment commentToUpdate = _context.CommentsData.Find(guid);
            if (commentToUpdate == null)
            {
                return null;
            }
            commentToUpdate.Body = updateData.NewBody;
            _context.SaveChanges();

            return commentToUpdate;
        }

        public Comment GetCommentByGuid(Guid guid)
        {
            return _context.CommentsData.Find(guid);
        }
    }
}
